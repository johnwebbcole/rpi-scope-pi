import { debug as Debug } from "debug";
const debug = Debug("scope-pi:state:nexstar");

const state = {
  ra: undefined,
  dec: undefined,
  updated: undefined
};

const mutations = {
  ra(state, value) {
    state.ra = value;
  },
  dec(state, value) {
    state.dec = value;
  },
  updated(state, value) {
    state.updated = value;
  }
};

const actions = {
  getRaDec: ({ commit }, { ra, dec }) => {
    commit("ra", ra);
    commit("dec", dec);
    commit("updated", new Date());
  },
  ra: ({ commit }, value) => commit("ra", value),
  dec: ({ commit }, value) => commit("dec", value)
};

const getters = {
  ra: state => state.ra,
  dec: state => state.dec,
  updated: state => state.updated
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
