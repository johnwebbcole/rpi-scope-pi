#!/bin/bash
set -e

find() {
  # echo "find '$1' '$2' '$3' '$4'"
  grep -s $1 $2 && {
    echo 'found $1 in $2'
  } || {
    echo 'not found'
    sudo sed -i "/$3/i $4" $2
  }
}

enable() {
  sudo sed -i "s/^#$1$/$1/" /boot/config.txt
}

disable() {
  sudo sed -i "s/^$1$/#$1/" /boot/config.txt
}

cmd_pitft_buttons() {
  DISPLAY=$1
  
  hash dtc 2> /dev/null || {
    echo "installing device-tree-compiler"
    sudo apt-get device-tree-compiler
  }
  
  dtc -W no-unit_address_vs_reg -@ -I dts -O dtb -o pitft28-gpio.dtb pitft28-gpio.dts
  sudo cp pitft28-gpio.dtb /boot/overlays
  
  sudo grep -q -s "device_tree_overlay=overlays/pitft28-gpio.dtb" /boot/config.txt || {
    sudo echo "device_tree_overlay=overlays/pitft28-gpio.dtb" | sudo tee -a /boot/config.txt
  }
  # ls -al /boot/overlays/pitft*
}

cmd_install_pitft() {
  hash fbcp 2> /dev/null || {
    echo "installing fbcp"
    sudo apt-get install cmake git
    git clone https://github.com/tasanakorn/rpi-fbcp
    cd rpi-fbcp/
    mkdir build
    cd build
    cmake ..
    make
    sudo install fbcp /usr/local/bin/fbcp
  }

  # copy fbcp into rc.local
  find fbcp /etc/rc.local "^exit 0$" "/usr/local/bin/fbcp &"

  # edit /boot/config.txt for pitft screen
  enable "disable_overscan=1"
  sudo sed -i "s/^#framebuffer_width=1280$/framebuffer_width=320/" /boot/config.txt
  sudo sed -i "s/^#framebuffer_height=720$/framebuffer_height=240/" /boot/config.txt
  sudo sed "s/^#dtparam=spi=on$/dtparam=spi=on/" /boot/config.txt
  enable "hdmi_force_hotplug=1"
  enable "hdmi_group=1"
  enable "hdmi_mode=1"
  enable "dtparam=i2c_arm=on"
  enable "dtparam=spi=on"
  #
  sudo grep -s pitft /boot/config.txt || {
    echo "dtoverlay=pitft22,speed=32000000,rotate=270,fps=25,touch-swapxy,touch-invx" | sudo tee -a /boot/config.txt
  }
}

cmd_terminal_font() {
  # set Terminus 6x12 as console font
  sudo sed -i "s/^CHARMAP=\"\"/CHARMAP=\"ISO-8859-1\"/" /etc/default/console-setup
  sudo sed -i "s/^CODESET=\"\"/CODESET=\"Lat15\"/" /etc/default/console-setup
  sudo sed -i "s/^FONTFACE=\"\"/FONTFACE=\"Terminus\"/" /etc/default/console-setup
  sudo sed -i "s/^FONTSIZE=\"\"/FONTSIZE=\"10x20\"/" /etc/default/console-setup
}

cmd_screen640x480() {
  sudo sed -i "s/^framebuffer_width=.*$/framebuffer_width=640/" /boot/config.txt
  sudo sed -i "s/^framebuffer_height=.*$/framebuffer_height=480/" /boot/config.txt
}

cmd_screen320x240() {
  sudo sed -i "s/^framebuffer_width=.*$/framebuffer_width=320/" /boot/config.txt
  sudo sed -i "s/^framebuffer_height=.*$/framebuffer_height=240/" /boot/config.txt
}

cmd_install_touch() {
  [ -f /etc/udev/rules.d/95-stmpe.rules ] || {
    echo "SUBSYSTEM==\"input\", ATTRS{name}==\"stmpe-ts\", ENV{DEVNAME}==\"*event*\", SYMLINK+=\"input/touchscreen\" \"" | sudo tee /etc/udev/rules.d/95-stmpe.rules

    sudo rmmod stmpe_ts
    sudo modprobe stmpe_ts

  }
}


cmd_disable_consoleblank() {
  sudo grep -s "consoleblank=0" /boot/cmdline.txt || {
    sudo sed -i '1 s/$/ consoleblank=0/' /boot/cmdline.txt
  }
}
# sudo systemctl start bluetooth


cmd_disable_camera_led() {
  sudo grep -s "^disable_camera_led=1" /boot/config.txt || {
    echo "disable_camera_led=1" | sudo tee -a /boot/config.txt
  }
}

cmd_disable_audio() {
  disable "dtparam=audio=on"
}

cmd_install_ser2net() {
  sudo apt-get install -y ser2net
  sudo sed -i '$ a\4000:raw:0:/dev/ttyUSB0:9600 NONE 1STOPBIT 8DATABITS' /etc/ser2net.conf
  sudo /etc/init.d/ser2net restart
}

# https://scribles.net/lightning-bolt-under-voltage-warning-on-raspberry-pi/
cmd_disable_undervoltage_warning() {
  sudo grep -s "^avoid_wawrnings=1" /boot/config.txt || {
    echo "avoid_warnings=1" | sudo tee -a /boot/config.txt
  }
}

cmd_help() {
  cat <<EOF
Picam installation and configuration.
USAGE: ./install.sh [command]

  COMMANDS:
$(declare -F | cut -d" " -f3 | grep "cmd_" | sed "s/cmd_/    /")
EOF
}



CMD=$1
shift
[ -z $CMD ] && help || cmd_$CMD $*
