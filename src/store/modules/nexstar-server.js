import { debug as Debug } from "debug";
const debug = Debug("scope-pi:state:nexstar");

const state = {
  ts: new Date(),
  heartbeat: undefined
};

const mutations = {
  heartbeat: (state, { ts }) => (state.heartbeat = ts)
};

const actions = {
  heartbeat: ({ commit }, { ts }) => commit("heartbeat", { ts }),
  touch: ({ commit }) => commit("heartbeat", { ts: Date.now() })
};

const getters = {
  started: state =>
    Date.now() - state.ts < 30000 ? "starting" : "disconnected",
  status: state =>
    state.heartbeat && Date.now() - state.heartbeat < 30000 && "listening",
  connected: (state, getters) => getters.status || getters.started
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
