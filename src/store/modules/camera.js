import { speeds } from "../../camera.js";
import { debug as Debug } from "debug";
const debug = Debug("scope-pi:state:camera");

const state = {
  iso: 800,
  shutter: "1/60",
  shutterSpeed: 16666,
  zoom: [],
  roi: {
    X: 0.0,
    Y: 0.0,
    W: 1.0,
    H: 1.0
  }
};

// mutations are operations that actually mutates the state.
// each mutation handler gets the entire state tree as the
// first argument, followed by additional payload arguments.
// mutations must be synchronous and can be recorded by plugins
// for debugging purposes.
const mutations = {
  iso(state, value) {
    state.iso = value;
  },
  shutter(state, value) {
    state.shutter = value;
    var idx = speeds.findIndex(s => s.name === value);
    state.shutterSpeed = speeds[idx].value;
  },
  zoomIn(state, quad) {
    state.zoom.push(quad);
  },
  zoomOut(state) {
    state.zoom.pop();
  },
  roi(state) {
    // 0 1
    // 2 3
    state.roi = state.zoom.reduce(
      (a, quad) => {
        debug("reduce", a, quad);
        return {
          X: quad == 0 || quad == 2 ? a.X : a.X + a.W / 2,
          Y: quad == 0 || quad == 1 ? a.Y : a.Y + a.H / 2,
          W: a.W / 2,
          H: a.H / 2
        };
      },
      { X: 0, Y: 0, W: 1.0, H: 1.0 }
    );
    debug("roi", { ...state.roi });
  }
};

const actions = {
  setIso: ({ commit }, value) => {
    debug("action setIso", value);
    return commit("iso", value);
  },
  setShutter: ({ commit }, value) => commit("shutter", value),
  zoomIn: ({ commit }, quad) => {
    debug("action zoomIn", quad);
    commit("zoomIn", quad);
    commit("roi");
  },
  zoomOut: ({ commit }) => {
    debug("action zoomOut");
    commit("zoomOut");
    commit("roi");
  }
};

// getters are functions
const getters = {
  iso: state => state.iso,
  shutter: state => state.shutter,
  zoom: state => state.zoom,
  roi: state => state.roi
};

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default {
  state,
  getters,
  actions,
  mutations
};
