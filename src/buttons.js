"use strict";
import { debug as Debug } from "debug";
const debug = Debug("scope-pi:buttons");
import { Gpio } from "onoff";

// export const pins = [17, 22, 23, 27];

export const pins = [5, 6, 12, 13, 16];
// export const pins = [5, 6, 7, 13, 16];
var clicks = new Array(pins.length).fill(0);
var clickTimer;
export default {
  install(Vue, { onChange, onClick }) {
    debug("install");
    var buttons = pins.map(function(pin, idx) {
      debug(`adding button:`, idx);
      var button = new Gpio(pin, "in", "both");
      debug(`adding button:`, idx, button.gpio);
      button.watch(function(err, value) {
        // down is 0, up is 1
        if (onClick) {
          if (value === 1) {
            clicks[idx]++;
            if (!clickTimer)
              clickTimer = setTimeout(() => {
                clickTimer = undefined;
                onClick(clicks);
                clicks.fill(0);
              }, 25);
          }
        }
        if (onChange) {
          return onChange({ err, button, value, idx, pin });
        }
        if (err) {
          return debug("** button.watch error", idx, button, err);
        }
        // console.log(`button ${pin}`, err || 'ok', value);
        // if (value === 1) {
        //
        // }
      });
      return button;
    });
    // Console.log('buttons', buttons);
    process.on("SIGINT", function() {
      buttons.forEach(function(button) {
        // console.log(`removing button:`, button.gpio);
        button.unexport();
        return button;
      });
    });
  },
};
