import { debug as Debug } from "debug";
const debug = Debug("scope-pi:command-mixin");

export default {
  data() {
    return {};
  },
  watch: {
    labels: {
      handler: function(value) {
        if (value) {
          this.$emit("setLabels", value);
        } else {
          debug("watch.labels - value is undefined");
        }
      },
      immediate: true
    }
  },
  methods: {
    button(bcode) {
      if (this.lookup) {
        var action = this.lookup[bcode];
        if (action) {
          if (this[action]) {
            this[action]();
          } else {
            this.$emit("setPage", action);
          }
        }
      }
    }
  }
};
