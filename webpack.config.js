var nodeExternals = require("webpack-node-externals");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "bundle.js",
    libraryTarget: "commonjs",
    path: path.resolve(__dirname)
  },
  target: "node",
  externals: [nodeExternals()],
  plugins: [new VueLoaderPlugin()],
  module: {
    // module.rules is the same as module.loaders in 1.x
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader"
      },
      {
        test: /\.js$/,
        loader: "buble-loader",
        include: path.join(__dirname, "src"),
        options: {
          objectAssign: "Object.assign"
        }
      }
    ]
  }
};
