import shell from "shelljs";
import { debug as Debug } from "debug";
const debug = Debug("scope-pi:settings");

export const initBrightness = function() {
  shell.exec("gpio -g mode 18 pwm");
};
export const setBrightness = function(value) {
  shell.exec(`gpio -g pwm 18 ${value}`);
};
