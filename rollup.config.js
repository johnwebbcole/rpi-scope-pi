import vue from "rollup-plugin-vue";
// import buble from "rollup-plugin-buble";
// import esbuild from "rollup-plugin-esbuild";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";

export default {
  input: "src/index.js",
  output: {
    format: "cjs",
    file: "bundle.js", // equivalent to --output},
  },
  plugins: [
    vue({
      htmlMinifier: {
        caseSensitive: true,
        keepClosingSlash: true,
      },
    }),
    commonjs(),
    json(),
    // buble({
    //   objectAssign: true,
    //   transforms: { asyncAwait: false },
    // }),
    // esbuild({}),
  ],
  external: [
    "blessed-vue",
    "blessed",
    "moment",
    "debug",
    "shelljs",
    "child_process",
    "vuex",
    "numeral",
    "onoff",
  ],
};
