var fs = require("fs");
var nodemon = require("nodemon");
var shell = require("shelljs");

nodemon({
  script: "bundle.js",
  ext: "js json",
  verbose: false,
  watch: ["bundle.js"]
  //   stdout: false
  // stderr: false
})
  .on("start", function() {
    console.log("App has started");
  })
  .on("quit", function() {
    console.log("App has quit");
    process.exit();
  })
  .on("restart", function(files) {
    console.log("App restarted due to: ", files);
  })
  .on("error", function(err) {
    // console.log("nodemon index.js error", err);
    shell.exec("clear");
    console.log("scope-pi shutting down");
    shell.exec(`sudo shutdown`);
  });
//   .on("readable", function() {
//     // the `readable` event indicates that data is ready to pick up
//     this.stdout.pipe(fs.createWriteStream("/dev/tty1"));
//     this.stderr.pipe(fs.createWriteStream("error.log"));
//   });
