import { debug as Debug } from "debug";
const debug = Debug("scope-pi:state:settings");

const state = {
  brightness: 1023
};

const mutations = {
  brightness: (state, { value }) => (state.brightness = value)
};

const actions = {
  brightness: ({ commit }, { value }) => commit("brightness", { value })
};

const getters = {
  brightness: state => state.brightness
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
