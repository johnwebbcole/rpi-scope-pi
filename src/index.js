import Vue from "blessed-vue";
import app from "./app.vue";
import store from "./store/index.js";
import { debug as Debug } from "debug";
const debug = Debug("scope-pi:index");
import mqtt from "./mqtt";

const Configstore = require("configstore");
// const pkg = require("../package.json");
import * as pkg from "../package.json";

// create a Configstore instance with an unique ID e.g.
// Package name and optionally some default values
const conf = new Configstore(pkg.name, { brightness: 1024 });

Vue.use(mqtt, { store });

const el = Vue.dom.createElement();

Vue.dom.append(el);

Vue.prototype.$conf = conf;

const instance = new Vue({
  name: "app",
  components: {
    app,
  },
  store,
  template: "<app />",
  mixins: [mqtt.mixin],
}).$mount(el);
