import { spawn } from "child_process";
import { debug as Debug } from "debug";
const debug = Debug("scope-pi:camera.js");

export const speeds = [
  {
    name: "1/250",
    value: 4000
  },
  {
    name: "1/100",
    value: 10000
  },
  {
    name: "1/60",
    value: 16666
  },
  {
    name: "1/30",
    value: 33333
  },
  {
    name: "1/10",
    value: 100000
  },
  {
    name: "1/4",
    value: 250000
  },
  {
    name: "1/2",
    value: 500000
  },
  {
    name: "1s",
    value: 1000000
  },
  {
    name: "2s",
    value: 2000000
  },
  {
    name: "3s",
    value: 3000000
  },
  {
    name: "6s",
    value: 6000000
  }
];

export const isos = [100, 200, 400, 800];

export const preview = function preview({
  stdout,
  stderr,
  iso,
  shutter,
  opacity = 255,
  roi = { X: 0.0, Y: 0.0, W: 1.0, H: 1.0 }
}) {
  debug({ iso, shutter, opacity });
  function shutterSpeed(shutter) {
    return speeds.filter(s => s.name === shutter)[0].value;
  }

  var res = [1640, 1232]; // rotated
  // var window = [Math.floor(1232 / 1640 * 440), 440];
  debug("window", window);
  var line = 480 / 12;
  var height = 480 - line * 2;
  var window = [Math.floor(1640 / 1232 * height), height];

  var params = [
    "-t",
    "0",
    "--verbose",
    "--stats",
    // "-rot",
    // "90",
    "--preview",
    `${640 - window[0]},${line * 1},640,${height}`,
    // `${640 - window[0]},0,${window[0]},${window[1]}`,
    "--mode",
    "4",
    "--awb",
    "sun",
    "--keypress",
    "--ISO",
    `${iso}`,
    "--shutter",
    `${shutterSpeed(shutter)}`,
    "--raw",
    "--quality",
    "100",
    "--latest",
    "latest.jpeg",
    "--thumb",
    "none",
    "-o",
    "image%04d.jpg",
    "--opacity",
    `${opacity}`,
    "--roi",
    `${roi.X},${roi.Y},${roi.W},${roi.H}`
    // "-ae",
    // "64,0xff,0x808000",
    // "-a",
    // "1234567890123456\n1234567890123456\n1234567890123456\n1234567890123456\n1234567890123456"
  ];

  var camera = spawn("raspistill", params);
  var resolveCapture;

  camera.stderr.on("data", data => {
    var msg = data.toString();
    // if (msg === "Press Enter to capture, X then ENTER to exit\n")
    //   status = "running";
    if (msg.startsWith("Finished capture") && resolveCapture)
      resolveCapture(msg);
    if (debug) debug(msg);
  });

  camera.stdout.on("data", data => {
    debug(data.toString());
  });

  //   camera.on("close", code => {
  //     // this.$off("camera-take");
  //     debug("camera closed", code);
  //     // this.preview = false;
  //     status = undefined;
  //     // this.$emit("camera-closed");
  //   });

  function close() {
    debug("close");
    return new Promise(resolve => {
      camera.on("close", code => {
        resolve(code);
      });
      camera.stdin.write("X\n");
    });
  }

  function take() {
    return new Promise(resolve => {
      resolveCapture = msg => {
        debug("resolveCapture", msg);
        resolveCapture = undefined;
        return resolve(msg);
      };
      camera.stdin.write("\n");
    });
  }
  return { camera, close, take, running: { iso, shutter, opacity } };
};
