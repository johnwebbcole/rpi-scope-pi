import BlessedVue from "blessed-vue";
import Vuex from "vuex";
import camera from "./modules/camera";
import nexstar from "./modules/nexstar";
import nexstarServer from "./modules/nexstar-server";

BlessedVue.use(Vuex);

export default new Vuex.Store({
  modules: {
    camera,
    nexstar,
    ["nexstar-server"]: nexstarServer
  }
});
