import { debug as Debug } from "debug";
const debug = Debug("scope-pi:mqtt");

var MQTT = require("async-mqtt");

export default {
  install(Vue, options) {
    var mqtt = MQTT.connect("mqtt://picam2.local");
    mqtt.on("connect", function(value) {
      debug("mqtt connected", value);
    });
    Vue.prototype.$mqtt = mqtt;
  },
  mixin: {
    mounted() {
      this.$mqtt
        .subscribe(["nexstar/getRaDec", "nexstar-server/+"])
        .then(function(granted) {
          debug("mqtt granted", granted);
        });

      this.$mqtt.on("message", (topic, payload, packet) => {
        debug("message", topic, payload.toString());
        this.$store.dispatch(topic, JSON.parse(payload.toString()));
        // if (this.$store.actions[topic]) debug("action found");
        // try {
        //   var args = JSON.parse(payload.toString());
        //   this.ra = Number.parseFloat(args.ra);
        //   this.dec = Number.parseFloat(args.dec);
        //   this.mqtt_update = new Date();
        // } catch (err) {
        //   console.error(
        //     `mqtt on message error topic: ${topic} payload: ${payload.toString()}`,
        //     err
        //   );
        // }
      });
    },
    beforeDestroy() {
      this.$mqtt
        .unsubscribe(["nexstar/getRaDec", "nexstar-server/+"])
        .then(function(err) {
          debug("unsubscribe", err || "ok");
        });
    }
  }
};
